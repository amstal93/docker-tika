# tags (before "\t") and build arguments (after "\t", separated by " " which cannot be utilised otherwise) of the Docker image variants to build
# for versions, see https://tika.apache.org/download.html https://svn.apache.org/viewvc/pdfbox/trunk/parent/pom.xml?view=co https://raw.githubusercontent.com/apache/tika/master/tika-parsers/pom.xml
1.24.1	TIKA_VERSION=1.24.1 IMAGEIO_CORE_VERSION=1.4.0 IMAGEIO_J2K_VERSION=1.3.0 SQLITE_VERSION=3.30.1 OCR_LANGS=ces,deu,fra,ita,pol,slk,spa

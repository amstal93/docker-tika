#!/bin/sh

# https://cwiki.apache.org/confluence/display/TIKA/TikaServer#TikaServer-RecursiveMetadataandContent

FILE_PATH="${1}"
FILE_NAME="${FILE_PATH##*/}"
FILE_TYPE=$(file --dereference --brief --mime-type "${FILE_PATH}")

URL="http://localhost:9998/rmeta"

OUT="${2}"
if [ -z "${OUT}" ]; then
	# HTML is better for a human-readable view
	OUT="html"
	# plain-text is better for subsequent processing
	#OUT="text"
	# ignore does not includ any text that is extracted from each document
	#OUT="ignore"
fi

URL="${URL}/${OUT}"

exec curl -i -X PUT --data-binary "@${FILE_PATH}" -H "Content-Disposition: attachment; filename=${FILE_NAME}" -H "Content-Type: ${FILE_TYPE}" "${URL}"

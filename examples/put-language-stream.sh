#!/bin/sh

# https://cwiki.apache.org/confluence/display/TIKA/TikaServer#TikaServer-LanguageResource

FILE_PATH="${1}"
FILE_NAME="${FILE_PATH##*/}"
FILE_TYPE=$(file --dereference --brief --mime-type "${FILE_PATH}")

URL="http://localhost:9998/language/stream"

exec curl -i -X PUT --data-binary "@${FILE_PATH}" -H "Content-Disposition: attachment; filename=${FILE_NAME}" -H "Content-Type: ${FILE_TYPE}" "${URL}"

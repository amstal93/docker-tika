# https://hub.docker.com/r/adoptopenjdk/openjdk11
# https://github.com/AdoptOpenJDK/openjdk-docker/blob/master/11/jdk/alpine/Dockerfile.hotspot.releases.slim
FROM adoptopenjdk/openjdk11:alpine-slim

MAINTAINER Marek Rychly <marek.rychly@gmail.com>

ARG DOWNLOAD_CACHE
ARG APACHE_ORIG="http://www-eu.apache.org/dist"
#ARG APACHE_MIRROR="ftp://mirror.hosting90.cz/apache"
ARG APACHE_MIRROR="http://archive.apache.org/dist"

# "eng" language training data is in tesseract-ocr
# see https://pkgs.alpinelinux.org/contents?file=*.traineddata&arch=x86_64
ARG OCR_LANGS="ces,deu,fra,ita,pol,slk,spa"

RUN true \
# debian:gdal-bin = gdal
# debian:tesseract-ocr-* = tesseract-ocr-data-*
# debian:xfonts-utils = bdftopcf font-util mkfontscale
# debian:fonts-freefont-ttf = ttf-freefont
# debian:fonts-liberation = ttf-liberation
# debian:ttf-mscorefonts-installer = msttcorefonts-installer
&& echo "${OCR_LANGS}" | tr ',' '\n' | sed 's/^/tesseract-ocr-data-/g' | xargs apk add --no-cache --update \
gdal tesseract-ocr \
bdftopcf font-util mkfontscale \
ttf-freefont ttf-liberation \
msttcorefonts-installer \
&& update-ms-fonts \
# clean up
&& rm -rf /tmp/* /var/tmp/* /var/cache/apk/*

# https://tika.apache.org/download.html
ARG TIKA_VERSION="1.24.1"
# https://svn.apache.org/viewvc/pdfbox/trunk/parent/pom.xml?view=co
ARG IMAGEIO_CORE_VERSION="1.4.0"
ARG IMAGEIO_J2K_VERSION="1.3.0"
# https://raw.githubusercontent.com/apache/tika/master/tika-parsers/pom.xml
ARG SQLITE_VERSION="3.30.1"

ENV \
TIKA_HOME="/opt/tika" \
TIKA_VERSION="${TIKA_VERSION}"

COPY scripts /

RUN true \
# make the scripts executable
&& chmod 755 /*.sh \
&& apk add --no-cache --update gnupg \
\
# download keys and trust them
&& ( [ -n "${DOWNLOAD_CACHE}" ] && cp -v "${DOWNLOAD_CACHE}/tika.KEYS" /tmp \
	|| wget -O /tmp/tika.KEYS "${APACHE_ORIG}/tika/KEYS" ) \
&& gpg --import /tmp/tika.KEYS \
&& echo "trust-model always" > ~/.gnupg/gpg.conf \
\
# download the package
&& ( [ -n "${DOWNLOAD_CACHE}" ] && cp -v "${DOWNLOAD_CACHE}/tika-server-${TIKA_VERSION}.jar" /tmp \
	|| wget -O /tmp/tika-server-${TIKA_VERSION}.jar "${APACHE_MIRROR}/tika/tika-server-${TIKA_VERSION}.jar" ) \
\
# download and verify signature
&& ( [ -n "${DOWNLOAD_CACHE}" ] && cp -v "${DOWNLOAD_CACHE}/tika-server-${TIKA_VERSION}.jar.asc" /tmp \
	|| wget -O /tmp/tika-server-${TIKA_VERSION}.jar.asc "${APACHE_ORIG}/tika/tika-server-${TIKA_VERSION}.jar.asc" ) \
&& for SIG in /tmp/*.asc; do gpg --verify "${SIG}" "${SIG%.asc}"; done \
\
# install
&& mkdir -p "${TIKA_HOME}" && mv "/tmp/tika-server-${TIKA_VERSION}.jar" "${TIKA_HOME}/tika-server.jar" \
\
# download and install dependencies that cannot be distributed for legal reasons (incompatible license)
&& MVN_SEARCH="https://search.maven.org/remotecontent?filepath=" \
&& wget -O "${TIKA_HOME}/jai-imageio-core.jar" "${MVN_SEARCH}com/github/jai-imageio/jai-imageio-core/${IMAGEIO_CORE_VERSION}/jai-imageio-core-${IMAGEIO_CORE_VERSION}.jar" \
&& wget -O "${TIKA_HOME}/jai-imageio-jpeg2000.jar" "${MVN_SEARCH}com/github/jai-imageio/jai-imageio-jpeg2000/${IMAGEIO_J2K_VERSION}/jai-imageio-jpeg2000-${IMAGEIO_J2K_VERSION}.jar" \
&& wget -O "${TIKA_HOME}/sqlite-jdbc.jar" "${MVN_SEARCH}org/xerial/sqlite-jdbc/${SQLITE_VERSION}/sqlite-jdbc-${SQLITE_VERSION}.jar" \
\
# clean up
&& apk del gnupg \
&& rm -rf /tmp/* /var/tmp/* /var/cache/apk/* /root/.gnupg

ENTRYPOINT ["/entrypoint.sh"]

HEALTHCHECK CMD /healthcheck.sh
